package com.benmosheron.bensofferserver.services

import cats.effect.Effect
import com.benmosheron.bensofferserver.data.access.OfferRetrieval
import com.benmosheron.bensofferserver.data.model.{OfferId, Offer}
import org.http4s.{HttpService, Response}
import org.http4s.dsl.Http4sDsl

class QueryOfferService[F[_] : Effect](offerRetrieval: OfferRetrieval) extends Http4sDsl[F] {
  type Behaviour = Offer => F[Response[F]]
  // Objects to interpret query string parameters
  object IncludeInactive extends OptionalQueryParamDecoderMatcher[Boolean]("includeInactive")
  object SearchTerms extends QueryParamDecoderMatcher[String]("terms")

  val service: HttpService[F] = HttpService[F] {
    case GET -> Root / "offer" / LongVar(id) :? IncludeInactive(inc) =>
      // Offer ID is encoded in the path
      val offerId = OfferId(id)
      // Optional query string parameter: includeActive (default to false)
      val includeActive = inc.getOrElse(false)
      // Behaviour of cancelled and expired offers depends on whether we should include inactive offers.
      // If we include them, then we should encode the offer, otherwise return an Ok with a message
      val cancelled: Behaviour = so => if (includeActive) encodeOffer(so) else okString("Offer cancelled")
      val expired: Behaviour = so => if (includeActive) encodeOffer(so) else okString("Offer expired")
      // Attempt to retrieve the offer
      offerRetrieval
        .getById(offerId)
        .fold(NotFound())(respondToOffer(cancelled, expired))
    case GET -> Root / "search" :? SearchTerms(terms) =>
      import org.http4s.circe.CirceEntityEncoder._
      Ok(Offer.toJsonArray(offerRetrieval.search(terms.split(" ").toList)))
  }

  private def respondToOffer(cancelled: Behaviour, expired: Behaviour)(so: Offer): F[Response[F]] = {
    so match {
      case offer if offer.cancelled => cancelled(offer)
      case offer if offer.expired => expired(offer)
      case offer => encodeOffer(offer)
    }
  }

  private def encodeOffer(so: Offer): F[Response[F]] = {
    import org.http4s.circe.CirceEntityEncoder._
    Ok(Offer.toJson(so))
  }

  private def okString(s: String) = {
    // Return our string as JSON, to keep all results from the query service of type application/json
    import org.http4s.circe.CirceEntityEncoder._
    import io.circe.Json
    Ok(Json.fromString(s))
  }

}