package com.benmosheron.bensofferserver.services

import cats.effect.Effect
import com.benmosheron.bensofferserver.data.access.OfferCreator
import com.benmosheron.bensofferserver.data.model.{OfferRequest, OfferId}
import io.circe.Json
import org.http4s.{HttpService, Response}
import org.http4s.dsl.Http4sDsl
import org.http4s.circe._
import io.circe.syntax._


class CreateOfferService[F[_]: Effect](da: OfferCreator) extends Http4sDsl[F] {
  val service: HttpService[F] = HttpService[F] {
    case req @ POST -> Root / "create" =>
       req.decode[Json](parseOffer(_).flatMap(da.createOffer).fold(failure)(success))
  }

  // Attempt to parse an offer from the entity.
  // I'm cutting corners with the logging here - in a production system, we would want to return this information
  // to the user (e.g. Incorrect Offer format...), and do some logging, rather than just ignoring the error.
  private def parseOffer(json: Json): Option[OfferRequest] = OfferRequest.fromJson(json).fold(_ => None, Some(_))

  // We require our offerId encoder in implicit scope
  import io.circe.generic.auto._
  private def success(offerId: OfferId): F[Response[F]] = Created(offerId.asJson)
  private def failure: F[Response[F]] = BadRequest("Could not create offer")
}
