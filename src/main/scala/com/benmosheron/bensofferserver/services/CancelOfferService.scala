package com.benmosheron.bensofferserver.services

import cats.effect.Effect
import com.benmosheron.bensofferserver.data.access.OfferCanceller
import com.benmosheron.bensofferserver.data.model.OfferId
import org.http4s.{HttpService, Response}
import org.http4s.dsl.Http4sDsl

// A simple service to cancel an existing offer
class CancelOfferService[F[_]: Effect](offerCanceller: OfferCanceller) extends Http4sDsl[F] {
  val service: HttpService[F] = HttpService[F] {
    case POST -> Root / "cancel" / LongVar(id) =>
      val offerId = OfferId(id)
      if(offerCanceller.cancel(offerId).unsafeRunSync()) success(offerId)
      else failure
  }
  private def success(offerId: OfferId): F[Response[F]] = Ok(s"Cancelled offer [${offerId.id}]")
  private def failure: F[Response[F]] = BadRequest("Could not cancel offer")
}
