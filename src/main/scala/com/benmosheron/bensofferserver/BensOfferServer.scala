package com.benmosheron.bensofferserver

import cats.effect.{Effect, IO}
import com.benmosheron.bensofferserver.data.access.{InMemoryOfferCanceller, InMemoryOfferCreator, InMemoryOfferRetrieval}
import com.benmosheron.bensofferserver.data.model.{OfferId, Offer}
import com.benmosheron.bensofferserver.services.{CancelOfferService, CreateOfferService, QueryOfferService}
import fs2.StreamApp
import org.http4s.HttpService
import org.http4s.server.blaze.BlazeBuilder

import scala.concurrent.ExecutionContext

// The application server - largely unchanged from the auto-generated HelloWorldServer provided by http4s.
// The server back end is "blaze" - the default provided by http4s.
// If you are unfamiliar with http4s, it is a very cool scala HTTP framework,
// which makes heavy use of functional constructs such as higher-kinded types.
// This project follows the architecture used in the quick start guide (see https://http4s.org/v1.0/).
object BensOfferServer extends StreamApp[IO] {
  import scala.concurrent.ExecutionContext.Implicits.global

  def stream(args: List[String], requestShutdown: IO[Unit]) = ServerStream.stream[IO]
}

object ServerStream {
  // This hash map will serve as our application's memory
  val memory = collection.mutable.HashMap[OfferId,Offer]()

  // Injected DA instances
  val offerCreator = new InMemoryOfferCreator(memory)
  val offerCanceller = new InMemoryOfferCanceller(memory)
  val offerRetrieval = new InMemoryOfferRetrieval(memory)

  // Services
  def createOfferService[F[_]: Effect]: HttpService[F] = new CreateOfferService[F](offerCreator).service
  def cancelOfferService[F[_]: Effect]: HttpService[F] = new CancelOfferService[F](offerCanceller).service
  def queryOfferService[F[_]: Effect]: HttpService[F] = new QueryOfferService[F](offerRetrieval).service

  def stream[F[_]: Effect](implicit ec: ExecutionContext) =
    BlazeBuilder[F]
      .bindHttp(8080, "0.0.0.0")
      .mountService(createOfferService, "/")
      .mountService(cancelOfferService, "/")
      .mountService(queryOfferService,"/")
      .serve
}
