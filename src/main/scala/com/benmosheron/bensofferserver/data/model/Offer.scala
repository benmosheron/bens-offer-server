package com.benmosheron.bensofferserver.data.model

import java.time.{LocalDateTime, ZoneId, ZonedDateTime}
import io.circe.Json
import scala.util.Try

/**
  * Represents an offer that is considered submitted at the time of instantiation
  */
case class Offer(offer: OfferRequest, submittedUtc: LocalDateTime = Offer.nowUtc, cancelled: Boolean = false) {
  import Offer.nowUtc
  private val expiry = submittedUtc.plusSeconds(offer.lifetimeSeconds)
  def expired: Boolean = nowUtc.isEqual(expiry) || nowUtc.isAfter(expiry)
  def cancel: Offer = this.copy(cancelled = true)
  def inactive: Boolean = expired || cancelled
  def active: Boolean = !inactive
}
object Offer{
  // Convenience method to get the current date/time in UTC
  def nowUtc: LocalDateTime = ZonedDateTime.now(ZoneId.of("Etc/UTC")).toLocalDateTime
  // Json utility methods
  // Currency encoder:
  import Currency._
  // asJson method:
  import io.circe.syntax._
  // All other case classes:
  import io.circe.generic.auto._
  // LocalDateTime:
  import io.circe.java8.time._
  def toJson(offer: Offer): Json = offer.asJson
  def fromJson(json: Json): Try[Offer] = json.as[Offer].toTry
  def toJsonArray(offers: List[(OfferId,Offer)]): Json = {
    offers
      .map(t => Json.obj("offerId" -> t._1.asJson, "offer" -> t._2.asJson))
      .asJson
  }
}
