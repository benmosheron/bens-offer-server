package com.benmosheron.bensofferserver.data.model

case class Price(amount: BigDecimal, currency: Currency) {
  require(amount >= 0, "Amount cannot be negative")
  require(amount < 78E12, s"Amount $amount exceeds or equals 78E12")
}
