package com.benmosheron.bensofferserver.data.model

import io.circe.Json
import scala.util.Try

/**
  * Represents an offer which has yet to be submitted, and therefore has no submitted date/time
  */
case class OfferRequest(
                  merchantId: MerchantId,
                  title: String,
                  description: String,
                  price: Price,
                  lifetimeSeconds: Int) {
  def submit: Offer = Offer(this)
}
object OfferRequest {
  // Json utility methods - we just need to keep track of a few implicit converters.
  // Beware intelliJ incorrectly assuming these are not needed!
  // Currency encoder:
  import Currency._
  // asJson method:
  import io.circe.syntax._
  // All other case classes:
  import io.circe.generic.auto._
  def toJson(offer: OfferRequest): Json = offer.asJson
  def fromJson(json: Json): Try[OfferRequest] = json.as[OfferRequest].toTry
}
// Value classes to identify the offer, and the merchant making an offer
case class OfferId(id: Long) extends AnyVal
case class MerchantId(id: Long) extends AnyVal

