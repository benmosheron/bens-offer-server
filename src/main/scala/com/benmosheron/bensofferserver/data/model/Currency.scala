package com.benmosheron.bensofferserver.data.model

import io.circe.{Decoder, HCursor}

// We will limit currencies to those defined here
sealed abstract case class Currency(id: String)
object Currency{
  def all: List[Currency] = List(EUR, GBP, USD)
  // Implicit JSON encoders and decoders, for de/serialization.
  // We only require custom logic for the Currency class
  import io.circe.{Encoder, Json}
  implicit val currencyEncoder: Encoder[Currency] = Encoder.instance { currency: Currency => Json.fromString(currency.id) }
  implicit val currencyDecoder: Decoder[Currency] = (c: HCursor) => {
    for (name <- c.as[String]) yield Currency.all.find(_.id == name).getOrElse(USD)
  }
}
object EUR extends Currency("EUR")
object GBP extends Currency("GBP")
object USD extends Currency("USD")
