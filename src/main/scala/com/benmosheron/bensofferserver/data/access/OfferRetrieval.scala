package com.benmosheron.bensofferserver.data.access

import com.benmosheron.bensofferserver.data.model.{OfferId, Offer}

// Simple class, abstracting over the process of querying for offers
trait OfferRetrieval {
  // Includes inactive offers
  def getById(offerId: OfferId): Option[Offer]
  // Simple search via title or description. Excludes inactive offers.
  def search(terms: List[String]): List[(OfferId,Offer)]
}
