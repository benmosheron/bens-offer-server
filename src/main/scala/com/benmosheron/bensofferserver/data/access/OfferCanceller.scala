package com.benmosheron.bensofferserver.data.access

import cats.effect.IO
import com.benmosheron.bensofferserver.data.model.OfferId

trait OfferCanceller {
  // We offer only success or failure as a result, this is an area of possible future work
  def cancel(offerId: OfferId): IO[Boolean]
}
