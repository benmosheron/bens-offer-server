package com.benmosheron.bensofferserver.data.access

import com.benmosheron.bensofferserver.data.model.{OfferId, Offer}

class InMemoryOfferRetrieval(memory: collection.mutable.HashMap[OfferId, Offer]) extends OfferRetrieval {
  override def getById(offerId: OfferId): Option[Offer] = memory.get(offerId)
  // Very basic - needs work to make this scalable, but it's simple, and good enough for our purposes.
  // We will include all active offers which match any of the search terms
  override def search(terms: List[String]): List[(OfferId,Offer)] = memory
    .filter{ case (_,so) => so.active}
    .filter{ case (_,so) => terms.exists(t => so.offer.title.contains(t) || so.offer.description.contains(t))}
    .toList
}
