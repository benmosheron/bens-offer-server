package com.benmosheron.bensofferserver.data.access

import com.benmosheron.bensofferserver.data.model.{OfferRequest, OfferId, Offer}

import scala.util.Random

class InMemoryOfferCreator(memory: collection.mutable.HashMap[OfferId, Offer]) extends OfferCreator {
  override def createOffer(offer: OfferRequest): Option[OfferId] = {
    // Generate a random ID
    val id = OfferId(math.abs(Random.nextLong()))
    // Submit the offer, commit to memory
    memory.put(id, offer.submit)
    // Return the ID
    Some(id)
  }
}
