package com.benmosheron.bensofferserver.data.access

import com.benmosheron.bensofferserver.data.model.{OfferRequest, OfferId}

trait OfferCreator {
  // Create an offer, returning the ID of the created offer upon success.
  // Returned None indicates failure. For demo purposes, no greater detail is included.
  def createOffer(offer: OfferRequest): Option[OfferId]
}
