package com.benmosheron.bensofferserver.data.access

import cats.effect.IO
import com.benmosheron.bensofferserver.data.model.{OfferId, Offer}

class InMemoryOfferCanceller(memory: collection.mutable.HashMap[OfferId, Offer]) extends OfferCanceller {
  override def cancel(offerId: OfferId): IO[Boolean] = {
    memory
      .get(offerId)
      .fold(IO.pure(false))(so => replaceWithCancelled(offerId,so,memory))
  }

  // If our offer ID is valid, we incur a side effect - that of replacing the stored offer with a cancelled instance
  import cats.effect.IO
  def replaceWithCancelled(offerId: OfferId, offer: Offer, memory: collection.mutable.HashMap[OfferId, Offer]): IO[Boolean] = {
    // note that we ignore the output of put, and return true regardless
    IO.pure(memory.put(offerId, offer.cancel)).map(_ => true)
  }

}
