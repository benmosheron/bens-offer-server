package com.benmosheron.bensofferserver.services

import cats.effect.IO
import com.benmosheron.bensofferserver.data.access.OfferCreator
import com.benmosheron.bensofferserver.data.model._
import org.http4s._
import org.http4s.implicits._
import org.scalatest.{BeforeAndAfter, FunSpec}

// Tests the offer creation service
class CreateOfferServiceSpec extends FunSpec with BeforeAndAfter{
  describe("CreateOfferService"){
    var factory: TestDAFactory = new TestDAFactory()
    before{
      // Each unit test will use a new instance of the test factory, with a clean hash map each run.
      factory = new TestDAFactory()
    }
    it("Responds with 201 Created upon success"){
      val response: Response[IO] = postAndRespond(factory.offerCreator)
      assert(response.status == Status.Created)
    }
    it("Responds with 400 Bad Request upon failure"){
      val response = postAndRespond(factory.offerCreatorFail)
      assert(response.status == Status.BadRequest)
    }
    it("Responds with 404 Not Found upon receiving an unexpected HTTP request"){
      // Let's use a GET
      val request = Request[IO](Method.GET, Uri.uri("/create"))
      val response = new CreateOfferService[IO](factory.offerCreator).service.orNotFound(request).unsafeRunSync()
      assert(response.status == Status.NotFound)
    }
    it("Returns the created offer ID"){
      val response: Response[IO] = postAndRespond(factory.offerCreator)
      val createdId = decodeOfferId(response)
      assert(factory.memory.contains(createdId), "The returned offer ID is not present")
    }
    it("Saves the posted offer"){
      val response: Response[IO] = postAndRespond(factory.offerCreator)
      val createdId = decodeOfferId(response)
      assert(factory.memory.get(createdId).map(_.offer).contains(testOffer))
    }
  }

  // The Offer our tests will use
  private val testOffer = CancelOfferServiceSpec.testOffer

  // Utility methods
  // POST to the create service, returning the response
  private def postAndRespond(offerCreator: OfferCreator) = getResponse(getPost, offerCreator)

  // Create a POST request to the create service, encoding an offer
  private def getPost: Request[IO] = {
    import org.http4s.circe._
    Request[IO](Method.POST, Uri.uri("/create")).withBody(OfferRequest.toJson(testOffer)).unsafeRunSync()
  }
  // Run the service
  private def getResponse(postOffer: Request[IO], offerCreator: OfferCreator) = {
    new CreateOfferService[IO](offerCreator).service.orNotFound(postOffer).unsafeRunSync()
  }
  // Extract the offer ID from the returned response
  private def decodeOfferId(response: Response[IO]): OfferId = {
    // Imports required to decode and deserialise the entity
    import org.http4s.circe.CirceEntityDecoder._
    import io.circe.generic.auto._
    response.as[OfferId].unsafeRunSync()
  }
}
