package com.benmosheron.bensofferserver.services

import cats.effect.IO
import com.benmosheron.bensofferserver.data.access.OfferRetrieval
import com.benmosheron.bensofferserver.data.model._
import io.circe.Json
import org.http4s._
import org.http4s.implicits._
import org.scalatest.{BeforeAndAfter, FunSpec}

class QueryOfferServiceSpec extends FunSpec with BeforeAndAfter {
  var factory: TestDAFactory = new TestDAFactory()
  describe("QueryOfferService"){
    before{
      factory = new TestDAFactory()
    }
    it("/offer/<non existent ID> => 404"){
      val response = getResponse(makeQueryRequest("-1"), factory.offerRetrieval)
      assert(response.status == Status.NotFound)
    }
    it("/offer/<expired ID> => 200 [Offer expired]"){
      // Add an expired offer
      val id = OfferId(1)
      factory.memory.put(id, CancelOfferServiceSpec.testOffer.copy(lifetimeSeconds = -1).submit)
      val response = getResponse(makeQueryRequest(id.id.toString), factory.offerRetrieval)
      import org.http4s.circe.CirceEntityDecoder._
      assert(response.status == Status.Ok)
      assert(response.as[Json].unsafeRunSync() == Json.fromString("Offer expired"))
    }
    it("/offer/<cancelled ID> => 200 [Offer cancelled]"){
      // Add a cancelled offer
      val id = OfferId(1)
      factory.memory.put(id, CancelOfferServiceSpec.testOffer.submit.cancel)
      val response = getResponse(makeQueryRequest(id.id.toString), factory.offerRetrieval)
      import org.http4s.circe.CirceEntityDecoder._
      assert(response.status == Status.Ok)
      assert(response.as[Json].unsafeRunSync() == Json.fromString("Offer cancelled"))
    }
    it("/offer/<active ID> => 200 (Offer JSON)"){
      // Add an offer
      val id = OfferId(1)
      val expected = CancelOfferServiceSpec.testOffer.submit
      factory.memory.put(id, expected)

      // Call the service, requesting the offer
      val response = getResponse(makeQueryRequest(id.id.toString), factory.offerRetrieval)
      import org.http4s.circe.CirceEntityDecoder._
      // The response body should be our offer
      val result = Offer.fromJson(response.as[Json].unsafeRunSync()).get

      assert(response.status == Status.Ok)
      assert(result == expected)
    }
    it("/offer/<expired ID>?includeInactive=true => 200 (Offer JSON)"){
      // Add an expired offer
      val id = OfferId(1)
      val expected = CancelOfferServiceSpec.testOffer.copy(lifetimeSeconds = -1).submit
      factory.memory.put(id, expected)

      // Call the service, requesting the offer (including inactive offers)
      val response = getResponse(makeQueryRequestIncludeInactive(id.id.toString), factory.offerRetrieval)
      import org.http4s.circe.CirceEntityDecoder._
      // The response body should be our offer
      val result = Offer.fromJson(response.as[Json].unsafeRunSync()).get

      assert(response.status == Status.Ok)
      assert(result == expected)
    }
    it("/offer/<cancelled ID>?includeInactive=true => 200 (Offer JSON)"){
      // Add a cancelled offer
      val id = OfferId(1)
      val expected = CancelOfferServiceSpec.testOffer.submit.cancel
      factory.memory.put(id, expected)

      // Call the service, requesting the offer (including inactive offers)
      val response = getResponse(makeQueryRequestIncludeInactive(id.id.toString), factory.offerRetrieval)
      import org.http4s.circe.CirceEntityDecoder._
      // The response body should be our offer
      val result = Offer.fromJson(response.as[Json].unsafeRunSync()).get

      assert(response.status == Status.Ok)
      assert(result == expected)
    }
    it("/offer/search?terms=test => 200 (JSON array of matching Offer and OfferIds)"){
      // Add a bunch of offers, and search for one of them.
      // An expired offer (should not appear)
      factory.memory.put(OfferId(1),CancelOfferServiceSpec.testOffer.copy(lifetimeSeconds = -1).submit)
      // A cancelled offer (should not appear)
      factory.memory.put(OfferId(2),CancelOfferServiceSpec.testOffer.submit.cancel)
      // A valid offer, which doesn't contain the word "test" (should not appear)
      factory.memory.put(OfferId(3),OfferRequest(MerchantId(-1),"hey","how ya doin",Price(10,USD),24).submit)
      // And finally, an offer which should appear
      factory.memory.put(OfferId(4),CancelOfferServiceSpec.testOffer.submit)
      val response = getResponse(makeSearchRequest(List("test")), factory.offerRetrieval)
      import org.http4s.circe.CirceEntityDecoder._
      val results = response.as[Json].unsafeRunSync().asArray.get
      assert(results.length == 1)
      // Let's also check we get back the offer we expect. I will just assert the ID to avoid writing deserialisation logic
      assert(decodeOfferId(results) == OfferId(4))
    }
  }

  private def makeQueryRequest(id: String): Request[IO] = {
    Request[IO](Method.GET, Uri.unsafeFromString(s"/offer/$id"))
  }

  private def makeQueryRequestIncludeInactive(id: String): Request[IO] = {
    Request[IO](Method.GET, Uri.unsafeFromString(s"/offer/$id?includeInactive=true"))
  }

  private def makeSearchRequest(terms: List[String]): Request[IO] = {
    Request[IO](Method.GET, Uri.unsafeFromString(s"/search?terms=${terms.mkString("+")}"))
  }

  // Call the service
  private def getResponse(req: Request[IO], offerRetrieval: OfferRetrieval): Response[IO] = {
    new QueryOfferService[IO](offerRetrieval).service.orNotFound(req).unsafeRunSync()
  }

  private def decodeOfferId(offersAndIds: Vector[Json]): OfferId = offersAndIds
    .headOption
    .flatMap(_.findAllByKey("offerId").headOption)
    .flatMap(_.findAllByKey("id").headOption)
    .flatMap(_.asNumber)
    .flatMap(_.toLong)
    .map(OfferId)
    .get
}
