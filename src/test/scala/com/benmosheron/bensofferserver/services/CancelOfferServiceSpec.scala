package com.benmosheron.bensofferserver.services

import cats.effect.IO
import com.benmosheron.bensofferserver.data.access.OfferCanceller
import com.benmosheron.bensofferserver.data.model._
import org.http4s.{Method, Request, Response, Status, Uri}
import org.http4s.implicits._
import org.scalatest.{BeforeAndAfter, FunSpec}

class CancelOfferServiceSpec extends FunSpec with BeforeAndAfter{
  import CancelOfferServiceSpec._
  describe("CancelOfferService"){
    var factory: TestDAFactory = new TestDAFactory()
    before{
      // Each unit test will use a new instance of the test factory, with a clean hash map each run.
      factory = new TestDAFactory()
    }
    it("/cancel/ => 404"){
      val response = postAndRespond("", factory.offerCanceller)
      assert(response.status == Status.NotFound)
    }
    it("/cancel/<non existent ID> => 400"){
      val response = postAndRespond("-1", factory.offerCanceller)
      assert(response.status == Status.BadRequest)
    }
    it("/cancel/<ID> => 200"){
      // Set up an offer we can cancel
      val id = OfferId(1)
      factory.memory.put(id, testOffer.submit)
      val response = postAndRespond(id.id.toString, factory.offerCanceller)
      assert(response.status == Status.Ok)
    }
    it("/cancel/<ID> (failure occurs) => 400"){
      val response = postAndRespond("-1", factory.offerCancellerFail)
      assert(response.status == Status.BadRequest)
    }
    it("Responds with 404 Not Found upon receiving an unexpected HTTP request"){
      // Let's use a GET
      val request = Request[IO](Method.GET, Uri.unsafeFromString(s"/cancel/1"))
      val response = new CancelOfferService[IO](factory.offerCanceller).service.orNotFound(request).unsafeRunSync()
      assert(response.status == Status.NotFound)
    }
    it("Cancels an offer (happy day)"){
      // Set up an offer we can cancel
      val id = OfferId(1)
      factory.memory.put(id, testOffer.submit)
      // Confirm that the offer exists and has not been cancelled
      assert(factory.memory.get(id).exists(!_.cancelled))
      postAndRespond(id.id.toString, factory.offerCanceller)
      // Check our offer has been cancelled
      assert(factory.memory.get(id).exists(_.cancelled))
    }
  }

  private def postAndRespond(id: String, offerCanceller: OfferCanceller) = getResponse(makeRequest(id), offerCanceller)

  // Create a POST request to cancel an offer
  private def makeRequest(id: String): Request[IO] = {
    Request[IO](Method.POST, Uri.unsafeFromString(s"/cancel/$id"))
  }

  // Call the service
  private def getResponse(req: Request[IO], offerCanceller: OfferCanceller): Response[IO] = {
    new CancelOfferService[IO](offerCanceller).service.orNotFound(req).unsafeRunSync()
  }
}
object CancelOfferServiceSpec{
  val testOffer = OfferRequest(MerchantId(-1),"test title", "test description",Price(100,GBP),1)
}
