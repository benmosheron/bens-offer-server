package com.benmosheron.bensofferserver.services

import cats.effect.IO
import com.benmosheron.bensofferserver.data.access._
import com.benmosheron.bensofferserver.data.model._

// Utility methods for building a DA to use in unit tests
class TestDAFactory {
  // Allow access to the underlying memory
  val memory = collection.mutable.HashMap[OfferId, Offer]()

  // To mock successful DA operations
  def offerCreator: OfferCreator = new InMemoryOfferCreator(memory)
  def offerCanceller: OfferCanceller = new InMemoryOfferCanceller(memory)
  def offerRetrieval: OfferRetrieval = new InMemoryOfferRetrieval(memory)
  // To mock the failures within individual DA classes
  def offerCreatorFail: OfferCreator = { _ => None}
  def offerCancellerFail: OfferCanceller = { _ => IO.pure(false)}
}
