package com.benmosheron.bensofferserver.data.model

import org.scalatest.FunSpec

class OfferRequestSpec extends FunSpec {
  import OfferRequestSpec._
  describe("Offer") {
    it("is not altered by serialisation") {
      val expected = standardOffer
      val actual = OfferRequest.fromJson(OfferRequest.toJson(expected)).get
      assert(actual == expected, "Offer was altered by serialisation round trip")
    }
  }
}
object OfferRequestSpec {
  // Expires in one hour
  def standardOffer = OfferRequest(MerchantId(-1), "test", "test", Price(1, GBP), 60*60)
}
