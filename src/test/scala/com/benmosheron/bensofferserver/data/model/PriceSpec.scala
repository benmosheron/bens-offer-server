package com.benmosheron.bensofferserver.data.model

import org.scalatest.FunSpec

class PriceSpec extends FunSpec{
  describe("Price"){
    it("requires a positive amount"){
      intercept[IllegalArgumentException](Price(-1,EUR))
    }
    it("requires an amount < 78E12"){
      intercept[IllegalArgumentException](Price(78.1E12,EUR))
    }
    it("happy day"){
      Price(1,EUR)
    }
  }
}
