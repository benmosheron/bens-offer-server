package com.benmosheron.bensofferserver.data.model

import org.scalatest.FunSpec

class OfferSpec extends FunSpec {
  import OfferRequestSpec._
  describe("Offer") {
    it("is expired if now >= submitted + lifetime") {
      assert(standardOffer.copy(lifetimeSeconds = 0).submit.expired)
    }
    it("is not expired if now < submitted + lifetime") {
      assert(!standardOffer.submit.expired)
    }
    it("is not altered by serialisation") {
      val expected = standardOffer.submit
      val actual = Offer.fromJson(Offer.toJson(expected)).get
      assert(actual == expected, "Offer was altered by serialisation round trip")
    }
  }

}
