bens-offer-server

A demo http4s server, which allows merchants to make limited time offers, which can be 
queried and cancelled.

The project is built using sbt, and organised as such:
* BensOfferServer - main server class
* services - package containing all available services
* data.access - package containing data access classes
* data.model - packages containing data model classes

This structure is mirrored between main and test folders. Unit tests utilise scalatest, and are run via `sbt test`.

No data is persisted, all offers are stored in memory (and therefore lost when the server restarts).
The following services are available:
* CreateOfferService
    * POST /create
    * The posted entity should be a JSONified OfferRequest instance
    * Responds with either:
        * 201 Content Created, returning offer ID, upon success
        * 400 Bad Request upon failure
* CancelOfferService
    * POST /cancel/\<offerId\>
    * Responds with:
        * 200 OK if the offer has been cancelled, or was already cancelled
        * 400 Bad Request upon failure
* QueryOfferService
    * GET /offer/\<id\>
        * Optional query string parameter: includeInactive={true,false}, which will allow retrieval of expired or cancelled offers
        * Responds with:
            * 200 Ok, returning the Offer, upon success
            * 200 Ok (Offer expired) if the offer has expired (unless includeInactive=true)
            * 200 Ok (Offer cancelled) if the offer has been cancelled (unless includeInactive=true)
            * 404 Not Found if the ID does not exist
    * GET /query/search
        * Required query string parameter: terms=\<space separated search terms\>
        * Only active (not expired, and not cancelled) offers can be searched for
        * Responds with 200 Ok, returning a JSON array of offer IDs and offers which match the search terms
 
Requests made outside of the above guidelines will receive a 404 Not Found.
 
About the data model:
* All times are UTC, an offer is considered submitted on instantiation
* Only EUR, GBP and USD currencies are supported. Invalid values will be replaced with USD
* Price must be positive, zero is allowed
* Price cannot exceed 78 trillion (to allow for the human race to band together for one particularly extravagant purchase, equal to gross world product in USD)
* An Offer can be cancelled. This is expressed via the `cancelled` field
* Negative and zero offer lifetimes are allowed, and can be used to create expired offers (included for testing)

A note on unit tests:
* Null pointers are not used (and there is no java interop), so I am not testing for them

Limitations:
* Corners have been cut with error reporting (errors are indicated by returning None), in a production system, the user would require more detailed knowledge of why an operation has failed.
* No effort has been made to provide a scalable data solution, as the focus is assumed to be on the code, rather than the infrastructure.
A first step towards improving this could be to paginate search results. A further step could be to implement a data access layer around a noSql data store, and segregate active vs inactive offers.

Start the server with `sbt run`. The server will accept HTTP requests over port 8080.

Example HTTP requests via curl

Creating an offer:

`curl -w "\n" -d '{"merchantId" : {"id" : 1001}, "title" : "test title", "description" : "test description", "price" : {"amount" : 100, "currency" : "GBP"}, "lifetimeSeconds" : 3600}' -H "Content-Type: application/json" -X POST http://[0:0:0:0:0:0:0:0]:8080/create`

Cancelling offer 934502651444926643:

`curl -w "\n" -X POST http://[0:0:0:0:0:0:0:0]:8080/cancel/934502651444926643`

Retrieving an offer by ID:

`curl -w "\n" http://[0:0:0:0:0:0:0:0]:8080/offer/6929942395495949960`

Retrieving an expired or cancelled offer by ID:

`curl -w "\n" http://[0:0:0:0:0:0:0:0]:8080/offer/934502651444926643?includeInactive=true`

Searching for an offer whose description or title contains either "cat" or "dog"

`curl -w "\n" http://[0:0:0:0:0:0:0:0]:8080/search?terms=cat+dog`